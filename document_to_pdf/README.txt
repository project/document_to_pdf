Document To PDF

Install the following modules using composer - 
1. phpoffice/phpword
2. setasign/fpdi
3. mpdf/mpdf
4. tmitry/xmlreader-extension

Enable the module after installing the above.
